package converter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author Luuk Velthuis on 28-4-2022
 * @project TempratureCoverter
 */

public class Converter extends HttpServlet {

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
    throws IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String docType = "<!DOCTYPE HTML>\n";
        String title = "Temperature Converter";
        out.println(docType + "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Temperature in Celcius: " +
                request.getParameter("degrees") + "\n" +
                "  <P>Temperature in Fahrenheit: " +
                celciusToFahrenheit(
                        Double.parseDouble(request.getParameter("degrees"))) +
                "</BODY></HTML>");

    }

    private double celciusToFahrenheit(double degreesCelcius) {
        return degreesCelcius * 1.8 + 32;
    }

}
